<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logic', 'Logic\LogicController@index')->name('home');

Route::post('/postHandle', 'Logic\HandleController@postHandle')->name('postHandle');
Route::post('/postCatch', 'Logic\CatchController@postCatch')->name('postCatch');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

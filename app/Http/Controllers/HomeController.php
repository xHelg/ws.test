<?php

namespace App\Http\Controllers;

use App\Listen;
use App\User;
use Illuminate\Support\Facades\Auth;

//use Illuminate\Http\Request;
//use GatewayClient\Gateway;
//use Workerman\Lib\Timer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        Gateway::sendToAll('asd');
        $Listen = Listen::orderby('id', 'desc')->limit(14)->get();
//        dump($Listen);
        $i = 0;
        foreach ($Listen as $value) {
            $list[$i] = $value->string;
            $intl[$i] = $value->integer;
            $i++;
        }

        $str = Auth::id().csrf_token().random_int(1,9999);
        $socket_toket = sha1($str);
        User::where('id', Auth::id())->update(['socket_token' => $socket_toket]);

//        echo  $socket_toket;

        return view('home')
            ->with('socket_token', $socket_toket)
            ->with('list', $list)
            ->with('intl', $intl);
    }
}

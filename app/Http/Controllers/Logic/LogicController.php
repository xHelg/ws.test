<?php

namespace App\Http\Controllers\Logic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use GatewayWorker\Gateway as GT;
use GatewayClient\Gateway as GTC;

class LogicController extends Controller
{
    function gtSend($data){
        return GTC::sendToAll($data);
    }

    public function index(Request $request) {
        echo "worked ".$request->getdata;
        $this->gtSend($request->getdata);
    }
}

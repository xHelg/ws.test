<?php

namespace App\Http\Controllers\Logic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\Logic\Logic;

class CatchController extends Controller
{
    public function postCatch(Request $request)
    {
        $var = new Logic();
        $var->addString($request->data);

        return redirect()->back();
    }
}



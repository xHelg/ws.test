<?php

namespace App\Http\Controllers\Logic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\Logic\Logic as Logic;

class HandleController extends Controller
{
    public function postHandle(Request $request)
    {
        return redirect()->back();
    }
}

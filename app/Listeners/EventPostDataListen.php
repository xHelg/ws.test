<?php

namespace App\Listeners;

use App\Events\EventPostData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventPostDataListen
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventPostData  $event
     * @return void
     */
    public function handle(EventPostData $event)
    {
        //
    }
}

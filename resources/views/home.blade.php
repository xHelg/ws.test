@extends('layouts.app')

@section('content')

    <script>
        window.onload = function() {
            ws = new WebSocket("ws://ws.test:7272");
            var wsdata = document.getElementById('wsdata');
            var wsbtn = document.getElementById('wsbtn');
            ws.onopen = function() {
                console.log("connection success");
                ws.send('{"type":"login", "content":"{{ $socket_token }}"}');
            };
            ws.onmessage = function(e) {
                console.log("recv message from server：" + e.data);
            };
            wsbtn.addEventListener("click", () => {
                ws.send(wsdata.value);
            })
        };
    </script>

    {{--ws.send("Привет");--}}


    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-12 justify-content-center">
        <div class="content col-4">
            <div>postCatch
                <form action="{{ route('postCatch') }}" method="post">
                    {{ csrf_field() }}
                    <input name="data" type="text">
                    <input type="submit" value="send" class="btn">
                </form>
            </div>
            <div>Handle Event
                <form action="{{ route('postHandle') }}" method="post">
                    {{ csrf_field() }}
                    <input name="data" type="text">
                    <input type="submit" value="send" class="btn">
                </form>
            </div>

            <div>WebSocket
                {{--<form action="" method="post">--}}
                    {{ csrf_field() }}
                    <input name="wsdata" type="text" id="wsdata">
                    <input type="submit" value="send" id="wsbtn">
                {{--</form>--}}
            </div>


        </div>
    </div>

    <div class="row justify-content-center mt-5">

        <div class="col-md-5">
            <div class="card">
                <div class="card-header">List Post</div>
                @foreach($list as $value)
                    <div class="card-body">
                        {{ $value }}
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-5">
            <div class="card">
                <div class="card-header">List Listeners</div>
            @foreach($intl as $value)
                <div class="card-body">
                    {{ $value }}
                </div>
            @endforeach
            </div>
        </div>

    </div>

    </div>


</div>
@endsection
